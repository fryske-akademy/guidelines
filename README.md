# guidelines

These are guidelines at the FA for the various ICT phases that help improve quality, reliability, maintainability.

In general: only develop software when it is functionaly necessary or it solves a problem.

Limit to a selected set of stable, supported frameworks, components and libraries

## shaping phase / project preparation

TODO

## software development phase

see [java architecture](java-architecture/README.md)

### use git versioning

* Source code, scripts, resources etc. must be versioned.
* Commit changes often with a clear, short description what has changed.
* All sources that form a release must be recognizable in git (for example via a tag or in a branche), it must be possible to fix bug from there.
* Repositories with multiple users have at least a master branche and a development branche.
* More complex projects use gitflow, see https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow, plugins available for build systems.

follow [git strategy](git.md) 

### use dependency management

* A solution to manage software dependencies must be used: maven, npm, pip, composer etc.
* Releases never depend on snapshots.

### work with releases

* When a solution works and is tested it may be released.
* Software that is generally useful should be published as open source (maven central,...).
* Software that is FA specific will be released in the FA repository.
* Only releases run in production. Bugfixes lead to a (minor) release.

### modularity

* Solutions should consist out of focused modules/libraries that cover one aspect.
* Use well defined, well documented interfaces that can be validated (inter and intra application communication)

### dos and don'ts

* no god classes
* no sql and/or commandline injection
* document complex / non intuitive code
* minimize dependencies
* minimize own coding of IO, streams, threads, concurrency, security, etc.
* use a logging framework and add useful logging
* no (database) driver dependencies
* only trusted, managed, preferably active dependencies
* keep data atomic, avoid parsing
* exercise restraint when applying ingenuity and tricks
* as little customization / configuration as possible

### static code analysis

* Static code analysis tools must be used: findbugs, checkstyle, phpstan, pylint, owasp,....

### ask for code reviews

### unit testing

* For crucial and/or tricky code (relying on certain formats etc.) unit tests should be written.

### profile your code

## software deployment phase

- script deployments (Dockerfile, bash, asadmin,...), the scripts should yield a ready to use solution, see [docker](docker.md)

## management phase

- propagate changes in environment to deployment scripts

### backups

* use automated backups, minimally for the past 7 days
* prevent backup data truncate existing data
    * pgdumpall with no arguments
* prevent backup data overwrite existing data
    * pgdumpall with no arguments
    * all tables have unique keys
    * psql -v ON_ERROR_STOP=1 < ....

### use issue tracking

- atomic issues
- preferably no anonymous issues
- review issues regularly and close/resolve
- keep it simple, issue systems tend to offer bus loads of functions / options

### logging, monitoring

# git strategy

The goal of this strategy is to ease cooperation and garantee independence.

## branche / release strategy

This setup enables working with your own release schedule, separate from the upstream. At the same time you can contribute and keep your code up to date.

- create a releases branche from dev
- don't develop in dev or your releases branche
- develop in feature branches created from dev, pull requests to upstream dev from there
- release from your own releases branche
    - adapt pom.xml in your releases branche (your fork git connection, distribution to your repo, release and gpg plugin)
        - this way you can merge incoming changes in pom from upstream
    - never merge your releases branche to your dev
    - use recognizable release tags (fa-1.7.0)

## working together in one feature branche

- this implies some git complexity, there will be multiple remotes, each local has to sync with the other remotes.
	- create a local branche pointing to a remote branche (name of the remote can be other than upstream)
		- ```git remote add upstream [giturl]```
		(example: ```git remote add upstream git@github.com:INL/BlackLab.git```)
		-  ```git fetch upstream```
		- ```git checkout -b localfeature upstream/feature```
		- ```git push origin localfeature``` pushes your changes to your remote
		- ```git pull upstream``` updates the localfeature branche

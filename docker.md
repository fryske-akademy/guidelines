# DOCKER

## why

- consistent, ready to use environments
	- otap
- better understanding of the parts of your apps
- fast startup and update
- scalability
- orchestration of focused images
- use and contribute to images available in repositories

## caveats

- harder to debug / troubleshoot
- be carefull not to overwrite or remove crucial volumes
- be aware that when upgrading a volume may still contain unwanted old data or even apps
- a docker-compose file can contain syntax for both compose and stack, i.e. deploy is only for stack
- variables in a Dockerfile are interpreted by SHELL, not by docker, i.e. ```RUN ["java","-jar", "$JAR"]``` doesn't work, ```RUN java -jar $JAR``` does
- bad error messages, for example just "Killed" when os has too little memory for a container
- fast paced development, what is supported, what is stable

Complete working setups can be found in the sibling hisgis, blacklab, foarkarswurdlist and exist-db repositories

## docker

### what it is / does

- building images
    - Dockerfile [reference](https://docs.docker.com/engine/reference/builder/))
- managing images in repository (push/pull..)
- running images
    - docker run, docker stack deploy

### guidelines

- separate build/run use Dockerfile/build only for build, stack/docker-compose.yml only for runtime
- use buildkit enhancements, especially ```build --secret...``` and ```RUN --mount=type=secret...```
  - at the top of your Dockerfile: ```# syntax=docker/dockerfile:experimental```
  - ```export DOCKER_BUILDKIT=1``` in your build env
- choose, and contribute to, stable, maintained base (FROM)
- read https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
- prefer docker stack to integrate focused images
- define path volumes under a root where every app has it's own directory
- user and group names in docker map to id's that may not be the same as on the host!
- give content on the host correct permissions so that the user in a container that runs processes can access it
- run containers as unpriv user, preferrably the user that your FROM defines
- confine each group of apps to it's own docker network 
- NOTE: network aliases can be refered to as host in for example jdbc config
- NOTE: COPY copies contents of directory, not the directory itself!!
- no passwords in images (only encrypted) => use docker secrets
- don't echo passwords, or use ARG for them, use external files and RUN --mount
- remove temp files

### backups

1. write a script that performs the backup
2. make the script available in the container (permissions!)
3. mount a volume in the container for the backups
4. write a cronscript for the host that calls the backup script in the container

### payara

- debugging: ```export PAYARA_ARGS=-d```, publish debug port 9009 in docker-compose.yml

## docker stack

### what it is / does

- define services from image(s)
- docker-compose.yml [reference](https://docs.docker.com/compose/compose-file/))
- you may choose to work with a repository for your images
	- run local repository: docker run -d -p 5000:5000 --restart=always --name registry registry:2
	- tag for local image: localhost:5000/test/secret:0.3
	- docker push localhost:5000/test/secret:0.3
- run services using stack deploy (or docker-compose up, which we do not prefer)

### guidelines

- using cpus under resources may cause extreme slowness
- the docker environment is responsible to provide a container with secrets
- ```docker secret create pw -``` creates a secret
- use path based volumes: ```<volume-root>/<project-root>/....```
- The 'version' entry in the compose file is important
	- determines what can be used
	- runtime will complain about unsupported version

## swarm / machines

### guidelines

- Primarily to limit the number of technologies we opt for docker stack instead of kubernetes

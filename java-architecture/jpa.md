# jpa

- [jpa](#markdown-header-jpa)
  * [entities and relations](#markdown-header-entities-and-relations)
    + [basics](#markdown-header-basics)
    + [relations](#markdown-header-relations)
    + [queries](#markdown-header-queries)
    + [auditing](#markdown-header-auditing)
  * [lifecycle Listeners](#markdown-header-lifecycle-listeners)
  * [crud operations](#markdown-header-crud-operations)
    + [attached / detached](#markdown-header-attached--detached)
    + [transactions](#markdown-header-transactions)
  * [persistence unit](#markdown-header-persistence-unit)

**[home](README.md)**  
Jpa is about translating java to rdbms and vise-versa. Read about it in the spec for 2.1, or better jpa 2.2.  
NOTE jpa stacktraces are often hard to interpret! Advises in this documentation help avoiding such stacktraces.
## entities and relations
Preferably extend AbstractEntity or AbstractWSEntity.
### basics
use as little annotations as you can  
wrappers or primitives: use wrappers to enable null as value, also for not null fields and id field and to prevent unwanted primitive defaults.  
Version: use primitive int with a default -1, this makes it a good candidate to check if an entity is stored in db or not.  
Use unique only when nullable is false
Use JoinColumn with ManyToOne, this will give you an entity_id foreign key

### relations
- collections
    - avoid using eager collections, only when you need the data anyway
    - Use a set for multiple eager collections in an entity
    - use a set to prevent Cartesian products, you may run into this when using collections and nesting entities
- bidirectional
    - avoid bidirectional relations as much as you can
- cascading
    - NOTE that cascaded operations do not trigger life cycle listeners on related entities
    - use with caution, especially useful to cascade remove to collections

### queries
Use named queries where you can, for performance and they are checked at application startup  
queries can be sped up by fetching only the fields you need in an Object[]  
CrudApi supports dynamic querying for entities based on smart parameters

### auditing
limit auditing to entities and fields that can be changed by users, don't audit fields that are changed in their own process
## lifecycle Listeners
remove from listener
When you need to access the database from a listener, use Util#getBean from jpaservices.  
NOTE review the javadoc for batch operations when using those!  
When cascading remove to a collection of entities with a back-reference remove the entity from the collection in a PreRemove
## crud operations
### attached / detached
If you have an attached entity and you are in a transaction, changes to it will be saved.  
Except for new entities you have to attach an entity before performing crud operations.  
As soon as you leave a bean method entities will be detached.
### transactions
- Preferably use CMT, JTA, container managed transactions!
- use @Transactional, works for both CDI and EJB
- Database related errors occur outside your method when using CMT! If you want them sooner you may consider calling flush.
- Transaction annotations only have effect when a client calls an annotated method, so Inject a bean holding transactional methods and call those! Calling a Transactional method from a bean itself in a non transactional context will not execute the method in a transaction nor will it start a new transaction!

## persistence unit
This explanation is for container (server) managed database access and transactions. A persistence.xml is used to define the 'client/application' side of the setup. The server defines the connection(s).

- client/application persistence.xml
    - see google, for example https://thorben-janssen.com/jpa-persistence-xml/
    - place this in main/resources/META-INF of your maven services module, example:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<persistence version="2.2" xmlns="http://xmlns.jcp.org/xml/ns/persistence" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/persistence              http://xmlns.jcp.org/xml/ns/persistence/persistence_2_2.xsd">
  <persistence-unit name="oat_unit" transaction-type="JTA">
    <provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>
    <jta-data-source>jdbc/oatresource</jta-data-source><!--looked up in server-->
    <class>org.fryske_akademy.jpa.RevisionInfo</class>
            .
            .
    <exclude-unlisted-classes>true</exclude-unlisted-classes>
    <shared-cache-mode>ENABLE_SELECTIVE</shared-cache-mode>
    <properties>
      <!--platform only needed for hibernate, not for eclipselink-->
      <property name="hibernate.transaction.jta.platform" value="org.hibernate.service.jta.platform.internal.SunOneJtaPlatform"/>
      <property name="hibernate.dialect" value="org.hibernate.dialect.PostgreSQL9Dialect"/>
      <!--<property name="hibernate.show_sql" value="true"/>-->
<!--      <property name="javax.persistence.schema-generation.scripts.action" value="create"/>-->
<!--      <property name="javax.persistence.schema-generation.scripts.create-target" value="oat-db.sql"/>-->
    </properties>
  </persistence-unit>
```
- server (explained for payara here)
    - driver  
    download jdbc driver and put it in ${PAYARA_DIR}/glassfish/domains/domain1/lib
    - jdbc-pool and jdbc-resource (can also be done via admin console)
```
# script execute with:
#    asadmin -u admin multimode --file /run/secrets/payarainit
#    see: https://javaee.github.io/glassfish/doc/4.0/reference-manual.pdf

create-password-alias oatdbpw

# pgdboat below is a docker network alias, may also be a regular hostname or ip.

create-jdbc-connection-pool --datasourceclassname org.postgresql.ds.PGConnectionPoolDataSource --isolationlevel=read-committed --restype javax.sql.ConnectionPoolDataSource --property user=oatweb:password=${ALIAS=oatdbpw}:serverName=pgdboat:databaseName=oat:portNumber=5432:driverClass=org.postgresql.Driver oatpool
create-jdbc-resource --connectionpoolid oatpool jdbc/oatresource

create-jdbc-connection-pool --datasourceclassname org.postgresql.ds.PGConnectionPoolDataSource --isolationlevel=read-committed --restype javax.sql.ConnectionPoolDataSource --property user=oatweb:password=${ALIAS=oatdbpw}:serverName=pgdboat:databaseName=oat:portNumber=5432:driverClass=org.postgresql.Driver:readOnly=true oatpoolro
create-jdbc-resource --connectionpoolid oatpoolro jdbc/oatresourcero

```
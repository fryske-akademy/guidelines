# primefaces

- [primefaces](#markdown-header-primefaces)
  * [things to learn](#markdown-header-things-to-learn)
  * [xhtml](#markdown-header-xhtml)
  * [controllers](#markdown-header-controllers)
  * [converters](#markdown-header-converters)
  * [validation](#markdown-header-validation)
  * [lazymodel](#markdown-header-lazymodel)
  * [performance](#markdown-header-performance)
  * [exception handling](#markdown-header-exception-handling)
  * [ajax](#markdown-header-ajax)
  * [multilingualism](#markdown-header-multilingualism)
  * [theming](#markdown-header-theming)

**[home](README.md)**

## things to learn

- primefaces showcase
- primefaces extensions showcase => sheet
- https://www.primefaces.org/documentation/
- jQuery
- [CrudApi](https://bitbucket.org/fryske-akademy/crudapi/src/master/)
- [EE 8, CDI 2, jpa 2.2, jsf 2.3](https://javaee.github.io/tutorial/toc.html)
- [example application](https://bitbucket.org/fryske-akademy/hisgis_oat_invoermodule)

## xhtml

- ui:param can be used when reusing snippets via ui:include, it's value can be a Java object
- an id attribute may eventually be prefixed by the id of its context, e.g. ```formid:buttonid```. In xhtml source, from the same context you can use ```buttonid```, from another context you must use ```formid:buttonid```. In jQuery you must use ```formid\\:buttonid```, escaping the :. If you don't specify an id jsf will generate one, this influences client side testing. It is often easier to refer a css class from javascript / jQuery, read about primefaces @() construct!.
- the widgetVar attribute causes a component to be available in javascript through PF(widgetVar), you can access the components js api. Sometimes PF() must be called from ```<p:remoteCommand delay="nnn"``` because the page isn't completely ready
- prevent submit when using enter by including a button as first element in forms and css that invisible:
```<p:commandButton styleClass="noEnter" tabindex="-1" value="" onclick="return false;"/>```
- when a combination of actionListener and update attribute doesn't seem to work, try `immediate="true"`
- when ```<p:ajax onsuccess``` doesn't seem to work, try ```<p:ajax oncomplete```
- support for new rows when using lazy model in datatables is supported in CrudApi, usage:
	- ```rowKey="#{ctrl.getRowKey(row.id)}"``` in your datatable definition
	- ```<p:ajax listener="#{ctrl.save}" event="rowEdit"``` inside the datatable
	- ```actionListener="#{ctrl.editNew('artikeltable')}" disabled="#{ctrl.newEntity!=null }"``` in for example a commandButton for adding a new row
	- implement newRowOn* methods to execute javascript that makes the new row visible (see lastPage function in example)
	- ```actionListener="#{ctrl.destroy(artikelController.toDelete())}" disabled="#{!row.isTransient() and row != ctrl.selected}" title="#{ctrl.deleteTitle()}"``` to delete a row from the datatable
- **NOTE** that all components used in a page do not automatically reflect serverside changes, also not when an EL expression points to an object that is changed!
- **NOTE** the rendered attribute may cause hard to discover side effects, often manipulating visibility is a better choice! For example not rendering the row selector causes your rowKey function not to be called.
- **NOTE** a command button inside a cell editor may fail silently, put it directly in a column
- **NOTE** subsequent requests can hide error messages!
- **NOTE** A commandButton inside a menuItem issues two identical requests!

## controllers

- Controllers are just CDI annotated beans (objects), holding (entity) state and methods you can use in EL expressions in you xhtml. Using SessionScoped often seems most convenient, but beware of state; only one controller should hold a reference to an object (entity), other controllers get a reference to that controller.  
- Wiring beans can be done using ```@Inject```, when your references become cyclic you can use lazy injection: ```Instance<class>```  
- The controller beans may be **passivation capable** (i.e. SessionScoped), in that case it should be **Serializable**, non Serializable fields such as stateless session beans should be **transient** and a private method **readObject** should initialize these fields, see **Util#getBean**.  
- Don't use ```@ManagedProperty``` but instead a Qualifier, for example ```@Named``` (though care must be taken with @Named)

## validation

- use ```javax.validation.constraints.*``` together with ```Util.formatConstraintException```
- do not rely on INTERPRET_EMPTY_STRING_SUBMITTED_VALUES_AS_NULL in web.xml, it is not reliable (yet?), instead use validation api


## converters

A converter (de)serializes an object. The String representation of an object could just be an id, but it is better to turn it into something meaningfull for users, that can be converted back to exactly one entity. Preferably annotated like this:
```
@FacesConverter(forClass = entity.class, managed = true)
@Named
@ApplicationScoped
```
When referencing an object in xhtml, the converter takes care of showing the user something meaningfull and it will translate user input to an object. **TIP**: when something doesn't work and you don't see an error anywhere look into your converters.
## lazymodel

As it's corresponding Controller this is also just a CDI bean holding state of a datatable.
- override #convertFilters if you need a default filter
- set case insensitivity to true in the constructor
- deal with datatyping and operations in #addToParamBuilder

## performance

Below measures can be considered to improve performance

-    no audit information in the gui, though supported by CrudApi
-    update as little components as possible
	- take a look at https://www.primefaces.org/showcase/ui/ajax/selector.xhtml
-    use @Cacheable second level cache where you can
-    don't use getUserPrincipal(), inject SecurityContext and use that
-    lazy loading unless you know you need the data anyhow
-    review generated queries, run them with explain, add indexes
-    partial submit by default:
```
    <context-param>
        <param-name>primefaces.SUBMIT</param-name>
        <param-value>partial</param-value>
    </context-param>
```
-    avoid declaring multiple ajax events of the same kind, this results in multiple calls
-    profile your app
-    cache EntityManagerFactory
-    limit auditing, don't audit fields (which may be collections) that are modified in a separate process
-    completely separate read/write, optimize (i.e. no transaction) read connection
-    look into partialSubmitFilter, in for example datatables by default the whole table is sent
-    consider batch operations, ([see batchSave](https://bitbucket.org/fryske-akademy/crudapi/src/5daa2a690d2492920cde88167fd3e3f8a1bef498/jpaservices/src/main/java/org/fryske_akademy/ejb/AbstractCrudService.java#lines-232) and [EntityException](https://bitbucket.org/fryske-akademy/crudapi/src/master/jpaservices/src/main/java/org/fryske_akademy/ejb/EntityException.java)), though tricky, read the javadoc!
-    When listeners need to access the database, use ```Util.getBean(Auditing.class);```, it has to be called from you lifecycle methods, don't worry, it is fast.
-    readonly connections without transaction for reads (= TxType.NOT_SUPPORTED, the default in CrudApi for reads).

## exception handling

- your java methods throw exceptions that make sense, PrimeExceptionHandler and  ajaxExceptionHandler make sure your users get informed. JsfUtil.handleException finds the deepest cause and show the message in it to users.
- **NOTE** (growl) messages may disappear because a next ajax call overwrites it. Growl has support for keeping messages, but that's not always what makes users happy. You may need to remember the previous message server side and redisplay.
- Special attention is necessary for decent handling of ajax and session timeout, see [ajaxtimeoutlistener](https://bitbucket.org/fryske-akademy/crudapi/src/master/primefacesgui/src/main/java/org/fryske_akademy/jsf/AjaxSessionTimeoutListener.java), all implemented in example project.

- configure in you faces-config.xml:
```
        <el-resolver>
            org.primefaces.application.exceptionhandler.PrimeExceptionHandlerELResolver
        </el-resolver>
    </application>
    <factory>
        <exception-handler-factory>
            org.fryske_akademy.jsf.exceptions.DeepestCauseExceptionHandlerFactory
        </exception-handler-factory>
    </factory>
    <lifecycle>
        <phase-listener>org.fryske_akademy.oat.jsf.AjaxSessionTimeoutListener</phase-listener>
    </lifecycle>
```
- in your template:
```
            <p:growl id="growl" life="5000" severity="info,warn,error" sticky="#{facesContext.validationFailed}" >
                <p:autoUpdate/>
            </p:growl>
             .
             .
            <p:ajaxExceptionHandler type="java.lang.Exception"
                                    update="exceptionDialog growl"/>

            <p:dialog id="exceptionDialog" header="Fout '#{pfExceptionHandler.message}'!" widgetVar="exceptionDialog"
                      height="500px" closable="true" >
                <h1>Details van de laatst opgetreden fout.</h1>
                <h2>Als de toepassing gewoon werkt gaat het wsl. om een fout waar de toepassing goed mee omgaat.</h2>
                <h2>Verversen met F5 kan helpen, opnieuw inloggen ook.</h2>
                <h2>stuur evt. de details naar edrenth at fryske-akademy nl</h2>
                Details: <h:outputText value="#{pfExceptionHandler.formattedStackTrace}" escape="false" /> <br />

            </p:dialog>
```
## ajax

CrudApi provides a AjaxSessionTimeoutListener that deals with session timeout in a friendly way. Easy to use, read the javadoc.

## multilingualism

- in you faces-config.xml:
```
    <application>
        <locale-config>
            <default-locale>nl_NL</default-locale>
            <supported-locale>fy_NL</supported-locale>
            <supported-locale>en_EN</supported-locale>
        </locale-config>
        <resource-bundle>
            <base-name>/Bundle</base-name>
            <var>bundle</var>
        </resource-bundle>
        .
        .
    <managed-bean>
        <managed-bean-class>org.fryske_akademy.jsf.SessionBean</managed-bean-class>
        <managed-bean-name>sessionBean</managed-bean-name>
        <managed-bean-scope>session</managed-bean-scope>
    </managed-bean>
```
- in your src/main/resources folder prepare Bundle_[language code].properties
- ```<f:view locale="#{sessionBean.locale}">``` in you template
- to change, use:
```
    <p:submenu label="#{sessionBean.language}">
        <c:forEach items="#{sessionBean.languages}" var="l">
            <p:menuitem value="#{l}" update="menuform" actionListener="#{sessionBean.lang(l)}"
                        onsuccess="location.replace(location.href)"/>
        </c:forEach>
    </p:submenu>
```
## theming

- in your web.xml:
```
    <context-param>
        <param-name>primefaces.THEME</param-name>
        <param-value>#{sessionBean.currentTheme}</param-value>
    </context-param>

```
- to change, use:
```
    <p:submenu label="thema">
        <c:forEach items="#{sessionBean.themes}" var="t">
            <p:menuitem value="#{t}" update="menuform" actionListener="#{sessionBean.theme(t)}"
                        onsuccess="location.replace(location.href)"/>
        </c:forEach>
    </p:submenu>

```

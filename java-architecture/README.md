# Architecture for Java solutions

- [Architecture for Java solutions](#markdown-header-architecture-for-java-solutions)
  * [Security](#markdown-header-security)
  * [maven](#markdown-header-maven)
  * [logging](#markdown-header-logging)
  * [modules that form an application](#markdown-header-modules-that-form-an-application)
    + [model: datamodel, interfaces, generated, helpers,..](#markdown-header-model-datamodel-interfaces-generated-helpers)
    + [cdi / ejb](#markdown-header-cdi-ejb)
    + [gui](#markdown-header-gui)
    + [ws](#markdown-header-ws)

In this "documentation by exception" you will not find what is already documented elsewhere, it focuses on the caveats discovered in development practice, on the things that will save a lot of searching/testing time.

This project describes how Java solutions at the FA are constructed. The architecture is supported via [CrudApi](https://bitbucket.org/fryske-akademy/crudapi). In general all apps are ee 8 based (ee 9.1 underway), spring isn't used, setup is modular, dependencies are minimized. Some more specific characteristics and approaches:

- ejb's (or just CDI beans, no declarative security then) for implementation of functionality, frontends get ejb's injected.
- no `@Remote ejb`
- prefer graphql (contract first via .graphqls) and optionally jsonb for services 
- (simple) scaling via docker, no clustered servers, remote ejb etc.
- use crudapi as much as possible

## Security

- EE declarative security annotations, used in EJB
- resources (database) referenced via jndi, made available in server
- no clear passwords in setup scripts or server config (aliases used)
- [jaas setup](jaas.md) in gui

## maven

We use maven as the basis for managing dependencies, code generation, building, packaging, releasing. As with everything else we stick to defaults and use as little features as we can, see [maven](maven.md) for details.

## logging

We use slf4j as logging abstraction and, when running under payara, slf4j-jdk as runtime dependency.

## modules that form an application
Below typical modules that form an application are described along with what they contain.
### model: datamodel, interfaces, generated, helpers,..

  - jpa entities for the datamodel, see [jpa](jpa.md)
      - full entities for use in gui
      - namedqueries
      - listeners
      - small (minimal #fields) cached entities for use in (web)services
      - Jsonb annotations for use in services
  - interfaces (no implementation, use these to inject EJB)
      - methods use jaxb generated data types
  - static helpers such as XMLHelper for schema, xsd, jaxbcontext, validation

### cdi / ejb
Separate beans for read and write. Separate bean modules for gui and webservices are suggested.

  - sometimes you want to inject a bean that results in cyclic wiring, solve that by using lazy injection: ```Instance<class>```.
  - persistence.xml
      - preferably jta transactions and jta datasource
      - connection properties go into the server
      - hibernate/envers variant for gui, a unit for read, a unit for write
      - eclipselink variant (faster) for webservices
  - stateless local EJB(s) that extend an abstract crudservice class from jpaservices
  - for transactions see [jpa](jpa.md)

### gui

  - controllers / converters / lazy loading / xhtml, see [primefaces](primefaces.md)

### ws
Preferred technology stack for services is graphql, otherwise jax-rs with jsonb. Jax-ws we do not use  and will be phased out

  - exclude envers from model:
  ```
  <exclusions>
    <exclusion>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-envers</artifactId>
    </exclusion>
</exclusions>
```
  - apache cxf with maven plugin for jaxb generation, no wsimport!
  - deprecated: jax-ws implemenation of generated port using bean interface for ws/rest
  - rest implemenation of generated port using bean interface for ws/rest and moxy, or
  - jsonb directly on jpa entities, care must be taken to keep the interface stable

Other applications may use a compile time dependency to the model and a runtime dependency to the ejb (which may require resources in server).

Example dependency graph:

![dependencies](deps.png)

# Jaas security setup

- [Jaas security setup](#markdown-header-jaas-security-setup)
  * [declarative security](#markdown-header-declarative-security)
  * [web.xml](#markdown-header-webxml)
  * [glassfish.xml](#markdown-header-glassfishxml)
  * [logout](#markdown-header-logout)
  * [session timeout](#markdown-header-session-timeout)

**[home](README.md)**

The server is responsible for requesting credentials, checking against a realm, providing a session, detecting session timeout, mapping groups to roles, etc. The application declares security contexts and roles and provides login page and error pages.
## declarative security

```
@Local({CrudWriteService.class})
@Stateless
public class OatServiceBean extends AbstractCrudServiceEnvers {

    @RolesAllowed(value = {EDITORROLE})
    public EntityManager getEntityManager() {...}

    @Interceptors({AccessInterceptor.class})
    public int batchDelete(Collection<? extends EntityInterface> t, Integer flushCount) {...}
    .
    .

@Interceptor
public class AccessInterceptor {

    @Inject
    private SecurityContext securityContext;

    @AroundInvoke
    public Object checkAccess(InvocationContext ctx) throws Exception {
    .
    .

```
Consider caching results from calls to SecurityContext which are a bit expensive.

## web.xml
define security context, security roles (ROLES NOT DECLARED IN THE APP WILL NOT BE PRESENT IN THE SECURITY CONTEXT!), which url's are protected, which are open, what is the login page, what the error page, declare the name of the realm in the server that will be used. Snippet:
```
    <security-constraint>
        <display-name>Secured</display-name>
        <web-resource-collection>
            <web-resource-name>app</web-resource-name>
            <description/>
            <url-pattern>/faces/*</url-pattern>
            <url-pattern>*.xhtml</url-pattern>
        </web-resource-collection>
        <auth-constraint>
            <description/>
            <role-name>editor</role-name>
            <role-name>gast</role-name>
        </auth-constraint>
        <user-data-constraint>
            <description/>
            <transport-guarantee>CONFIDENTIAL</transport-guarantee>
        </user-data-constraint>
    </security-constraint>
    <security-constraint>
        <web-resource-collection>
            <web-resource-name>Open Content</web-resource-name>
            <url-pattern>/faces/javax.faces.resource/*</url-pattern>
            <url-pattern>/errors.xhtml</url-pattern>
            <url-pattern>/timeout.xhtml</url-pattern>
        </web-resource-collection>
        <user-data-constraint>
            <description/>
            <transport-guarantee>CONFIDENTIAL</transport-guarantee>
        </user-data-constraint>
    </security-constraint>
    <login-config>
        <auth-method>FORM</auth-method>
        <realm-name>auth</realm-name>
        <!-- This realm corresponds to the name of a realm created in the server.
        The server realm-name in it's turn determines the technology used for
        authentication/authorization (such as jdbc, file, ldap)-->
        <form-login-config>
            <form-login-page>/login.xhtml</form-login-page>
            <form-error-page>/error.xhtml</form-error-page>
        </form-login-config>
    </login-config>
    <security-role>
        <description/>
        <role-name>gast</role-name>
    </security-role>
    <security-role>
        <description/>
        <role-name>editor</role-name>
    </security-role>
    <security-role>
        <description/>
        <role-name>beheer</role-name>
    </security-role>
```
## glassfish.xml
roll mapping, which groups (AD groups for example) will be mapped to which role(s)
```
  <security-role-mapping>
    <role-name>gast</role-name>
    <group-name>gast</group-name>
  </security-role-mapping>
  <security-role-mapping>
    <role-name>editor</role-name>
    <group-name>oat</group-name>
  </security-role-mapping>

```
## logout
The sessionBean from CrudApi provides a logout() function

## session timeout
CrudApi provides AjaxSessionTimeoutListener that can be declared in faces-config.xml to handle session timeout in a friendly way.

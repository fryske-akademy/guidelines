# maven

**[home](README.md)**  

Maven is about reliability and control as it comes to dependencies, building, testing, packaging and more when developing and publishing software.
Beware, there are a lot of bloated, ill designed pom's hanging around on the internet containing unnecessary fixes, exclusions, transformations and the like. pom's should be small and understandable.

## guidelines

- use explicit versions for dependencies and plugins
- minimize dependencies
- parent pom contains dependencyManagement, modules, build plugins and project meta information such as properties, distributionManagement, licences, repositories, scm, developers, etc.
- ideally child pom's only contain dependencies
- know about and use scopes
